#Servidor TCP
import socket
#Endereço IP do servidor
HOST = ''
#Porta que o servidor vai escutar 
PORT = 5002
tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
orig = (HOST, PORT)
tcp.bind(orig)
tcp.listen(1)
while True:
    con, cliente = tcp.accept()
    print('Conectado por', cliente)
    while True:
        msg = con.recv(1024)
        if not msg: break
        print(msg)
    print('Finelizando conexao do cliente', cliente)
con.close()